package cache

import (
	"gitee.com/aesoper/cache/factory"
	"gitee.com/aesoper/cache/redis"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	cache, err := factory.NewCache("memcache")
	assert.Nil(t, err)
	if err != nil {
		return
	}

	putErr := cache.Put("test", "111", time.Minute*5)
	assert.Nil(t, putErr)

	get, err := cache.Get("test")
	assert.Nil(t, err)
	assert.Equal(t, "111", get)
}

func TestNew2(t *testing.T) {
	cache, err := New(Cfg{
		Driver: "redis",
		Redis: redis.Options{
			Addr: "localhost:6379",
		},
	})
	assert.Nil(t, err)
	if err != nil {
		return
	}

	putErr := cache.Put("test", "111", time.Minute*5)
	assert.Nil(t, putErr)

	get, err := cache.Get("test")
	assert.Nil(t, err)
	assert.Equal(t, "111", string(get))
}
